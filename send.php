<?php

/* Для начала нам необходимо инициализировать данные, необходимые для составления запроса. */
$subdomain  = 'antonio3452acc'; #Наш аккаунт - поддомен
$user       =   array(
                        'USER_LOGIN'    =>  'antonio3452acc@gmail.com',
                        'USER_HASH'     =>  '9c1d3bc21e657debe694a2effd55efcc66f52c2d'
                    );
$name       = $_REQUEST['name'];
$phone      = $_REQUEST['phone'];
$email      = $_REQUEST['email'];

$phoneFieldId = '355667'; //ID поля "Телефон" в amocrm
$emailFieldId = '355669'; //ID поля "Email" в amocrm
$responsibleId = '28709218'; //ID Ответственного сотрудника в amocrm

$link = 'https://' . $subdomain . '.amocrm.ru/private/api/auth.php?type=json';

$contactInfo = findContact($subdomain, $email);
    $idContact = $contactInfo['idContact'];
    if ($idContact != null) {
        $idDeal[] = addDeal($dealName, $dealStatusID, $dealSale, $responsibleId, $dealTags, $subdomain);
        if ($idDeal != null) {
            if (!empty($contactInfo['idLeads'])) {
                foreach ($contactInfo['idLeads'] as $idLeads) {
                    $idDeal[] = $idLeads;
                }
            }
            echo $idDeal;
            editContact($idContact, $idDeal, $subdomain);
        }
    } else {
        addContact($name,$responsibleId,$phoneFieldId,$phone,$emailFieldId,$email, $subdomain, $contactTags);
        echo ("ELSE!");  
    }
   
    function findContact($subdomain, $email)
    {
        $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts/?query=' . $email;
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            if ($code != 200 && $code != 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            }
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }

        $Response = json_decode($out, true);
        $Response = $Response['_embedded']['items'][0];
        $Response['idContact'] = $Response['id'];
        $Response['idLeads'] = $Response['leads']['id'];
        return $Response;
    }


function addContact($name, $responsibleId, $phoneFieldId, $phone, $emailFieldId, $email, $subdomain, $contactTags)
    {
        $contacts['add'] = array(
            array(
                'name' => $name,
                'responsible_user_id' => $responsibleId,
                'created_by' => $responsibleId,
                'created_at' => time(),
                'tags' => $contactTags, //Теги
                'custom_fields' => array(
                    array(
                        'id' => "$phoneFieldId",
                        'values' => array(
                            array(
                                'value' => "$phone",
                                'enum' => "MOB"
                            )
                        )
                    ),
                    array(
                        'id' => $emailFieldId,
                        'values' => array(
                            array(
                                'value' => $email,
                                'enum' => "WORK"
                            )
                        )
                    ),
                ),
            )
        );
        $link = '';
        $link .= 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts';

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);

        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            if ($code != 200 && $code != 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            }
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }

        $Response = json_decode($out, true);
        $Response = $Response['_embedded']['items'][0]['id'];
        return $Response;
    }



    function editContact($idContact, $idDeal, $subdomain)
    {
        $contacts['update']  = array(
                                    array(
                                        'id' => $idContact,
                                        'updated_at' => time(),
                                        'leads_id' => $idDeal,
                                    )
                                );

        $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/contacts';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($contacts));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            if ($code != 200 && $code != 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            }
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
        $Response = json_decode($out, true);
        $Response = $Response['_embedded']['items'][0]['id'];
            return $Response;
    }

        
    function addDeal($dealName, $dealStatusID, $dealSale, $responsibleId, $dealTags, $subdomain)
    {
        $leads['add'] = array(
                            array(
                                'name' => $dealName,
                                'created_at' => time(),
                                'status_id' => $dealStatusID,
                                'sale' => $dealSale,
                                'responsible_user_id' => $responsibleId,
                                'tags' => $dealTags
                            ),
        );

        $link = 'https://' . $subdomain . '.amocrm.ru/api/v2/leads';
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_USERAGENT, 'amoCRM-API-client/1.0');
        curl_setopt($curl, CURLOPT_URL, $link);
        curl_setopt($curl, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($leads));
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($curl, CURLOPT_HEADER, false);
        curl_setopt($curl, CURLOPT_COOKIEFILE, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_COOKIEJAR, __DIR__ . '/cookie.txt');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        $out = curl_exec($curl);
        $code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $code = (int)$code;
        $errors = array(
            301 => 'Moved permanently',
            400 => 'Bad request',
            401 => 'Unauthorized',
            403 => 'Forbidden',
            404 => 'Not found',
            500 => 'Internal server error',
            502 => 'Bad gateway',
            503 => 'Service unavailable'
        );
        try {
            if ($code != 200 && $code != 204) {
                throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undescribed error', $code);
            }
        } catch (Exception $E) {
            die('Ошибка: ' . $E->getMessage() . PHP_EOL . 'Код ошибки: ' . $E->getCode());
        }
        $Response = json_decode($out, true);
        $Response = $Response['_embedded']['items'][0]['id'];
        return $Response;
    }